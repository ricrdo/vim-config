" Yowish Statusline Colors  {{{
call yowish#SetConfig()
let s:color = g:yowish.colors

" Highlighting function from yowish colorscheme
fun! s:Hi(groupName, bgColor, fgColor, opt)
	let l:bg = type(a:bgColor) ==# type('') ? ['NONE', 'NONE' ] : a:bgColor
	let l:fg = type(a:fgColor) ==# type('') ? ['NONE', 'NONE'] : a:fgColor
	let l:mode = ['gui', 'cterm']
	let l:cmd = 'hi ' . a:groupName
	for l:i in (range(0, len(l:mode)-1))
		let l:cmd .= printf(' %sbg=%s %sfg=%s ',
					\ l:mode[l:i], l:bg[l:i],
					\ l:mode[l:i], l:fg[l:i]
					\ )
	endfor
	execute l:cmd
endfun

function! StatuslineColor()
	if (mode() =~# '\v(n|no)')
		call s:Hi('StatusLine', s:color.background, s:color.comment, 'NONE')
	elseif (mode() =~# '\v(v|V)' || g:currentmode[mode()] ==# 'V·Block' || get(g:currentmode, mode(), '') ==# 't')
		call s:Hi('StatusLine', s:color.background, s:color.yellow, 'NONE')
	elseif (mode() ==# 'i')
		call s:Hi('StatusLine', s:color.background, s:color.red, 'NONE')
	else
		call s:Hi('StatusLine', s:color.background, s:color.comment, 'NONE')
	endif
	return ''
endfunction
" }}}


