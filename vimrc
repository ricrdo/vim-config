" vim: nocp fen fdm=marker fdl=0 tw=78 cc=78 noet
" ===========================================================================
" Description: @tun's vimrc
" Author: Ricardo Tun
" URL: https://github.com/tun/vim-config
" ===========================================================================
" Init {{{
" Plug.vim {{{
augroup plugins
	autocmd!
	" Setting up {{{
	set nocompatible
	set modeline
	if empty(glob('~/.vim/autoload/plug.vim'))
		silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
					\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
		autocmd VimEnter * PlugInstall
	endif
	command! PU PlugInstall | PlugUpdate | PlugUpgrade
	" }}}
	" Plugins {{{
	call plug#begin('~/.vim/plugged')
	Plug 'andymass/vim-matchup'
	Plug 'haya14busa/is.vim'
	Plug 'joereynolds/vim-minisnip'
	Plug 'mattn/emmet-vim'
	Plug 'w0rp/ale'
	Plug 'kshenoy/vim-signature'
	Plug 'mhinz/vim-signify'
	Plug 'tpope/vim-dispatch'
	Plug 'tpope/vim-eunuch'
	Plug 'tpope/vim-fugitive'
	Plug 'tpope/vim-surround'
	Plug 'AndrewRadev/splitjoin.vim'
	Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
	Plug 'junegunn/fzf.vim'
	Plug 'junegunn/gv.vim'
	Plug 'drzel/vim-line-no-indicator'
	Plug 'jiangmiao/auto-pairs'
	Plug 'MarcWeber/vim-addon-local-vimrc'
	" Syntax
	Plug 'elixir-editors/vim-elixir'
	Plug 'othree/yajs.vim', {'for': 'javascript'}
	Plug 'othree/javascript-libraries-syntax.vim', {'for': 'javascript'}
	Plug 'othree/html5.vim', {'for': 'html'}
	Plug 'mustache/vim-mustache-handlebars', {'for': 'mustache'}
	Plug 'digitaltoad/vim-pug', {'for': 'pug'}
	Plug 'posva/vim-vue'
	" Local machine configuration
	if filereadable(glob("~/.vimrc.local"))
		source ~/.vimrc.local
	endif
	call plug#end()
	" }}}
augroup end
" }}}
set omnifunc=syntaxcomplete#Complete
set autoread
set nobackup
set noswapfile
set undofile
set undodir=~/.vim/undo
set history=500
set encoding=utf-8
set iskeyword+=-
set formatoptions-=cro
set fileformat=unix
set nostartofline
set noendofline
set nojoinspaces
set ttyfast
set ttimeout
set ttimeoutlen=0
set noautochdir
set lazyredraw
set backspace=indent,eol,start
set hidden
set mouse=nv
" }}}
" Indentation {{{
set autoindent
set smartindent
set copyindent
set linebreak
set breakindent
" }}}
" Tabs {{{
set noexpandtab
set smarttab
set tabstop=2
set softtabstop=2
set shiftwidth=2
set shiftround
" }}}
" Search {{{
set incsearch
set hlsearch
set showmatch
set ignorecase
set smartcase
" }}}
" Wildmenu {{{
set wildmenu
set wildmode=full
set wildignore+=*.png,*.jpg,*.jpeg,*.gif
set wildignore=__init__.py,*.pyc,*.db
set wildignore+=dist,node_modules
" }}}
" Languages/Filetypes {{{
" Markdown {{{
func! Foldexpr_markdown(lnum)
	let l1 = getline(a:lnum)
	if l1 =~ '^\s*$'
		" ignore empty lines
		return '='
	endif
	let l2 = getline(a:lnum+1)
	if  l2 =~ '^==\+\s*'
		" next line is underlined (level 1)
		return '>1'
	elseif l2 =~ '^--\+\s*'
		" next line is underlined (level 2)
		return '>2'
	elseif l1 =~ '^#'
		" current line starts with hashes
		return '>'.matchend(l1, '^#\+')
	elseif a:lnum == 1
		" fold any 'preamble'
		return '>1'
	else
		" keep previous foldlevel
		return '='
	endif
endfunc
autocmd FileType markdown setlocal textwidth=74
			\ conceallevel=0
			\ foldexpr=Foldexpr_markdown(v:lnum)
			\ foldmethod=expr
let g:markdown_fenced_languages =
			\ ['css', 'python', 'erb=eruby', 'ruby',
			\ 'javascript', 'js=javascript', 'json=javascript',
			\ 'sass', 'xml', 'html']
" }}}
" Python {{{
augroup py_autocmd
	autocmd!
	autocmd FileType python setlocal textwidth=80
				\ colorcolumn=80 |
				\ match ErrorMsg '\%>80v.\+'
augroup END
" }}}
" JavaScript {{{
let g:used_javascript_libs = 'underscore,react,handlebars'
augroup js_autocmd
	autocmd BufNewFile,BufRead *.es6 set filetype=javascript
	autocmd FileType javascript setlocal shiftwidth=2
				\ softtabstop=2
				\ foldmethod=syntax
				\ foldlevelstart=1 |
				\ syntax region foldBraces start=/{/ end=/}/
				\ transparent fold keepend extend 
augroup END
" }}}
" JSON {{{
let g:vim_json_syntax_conceal = 0
augroup json_autocmd
	autocmd!
	autocmd FileType json set autoindent
				\ formatoptions=tcq2l
				\ textwidth=74
				\ shiftwidth=2
				\ softtabstop=2
				\ tabstop=8
				\ expandtab
				\ foldmethod=indent
augroup END
" }}}
" HTML/PUG {{{
augroup html_autocmd
	autocmd FileType html,pug setlocal shiftwidth=2
				\ softtabstop=2
				\ textwidth=0
				\ | :normal zR
augroup END
" }}}
" Mustache & Handlebars
let g:mustache_abbreviations = 1
" Pug
autocmd BufNewFile,BufReadPost *.pug set filetype=pug
" }}}
" Plugin's settings " {{{
" Signify {{{
let g:signify_vcs_list = ['git', 'hg']
nmap <leader>gj <plug>(signify-next-hunk)
nmap <leader>gk <plug>(signify-prev-hunk)
" }}}
" FZF {{{
let g:fzf_layout = { 'right': '~40%' }
autocmd! FileType fzf
autocmd  FileType fzf set laststatus=0
			\| autocmd BufLeave <buffer> set laststatus=2
nnoremap <Leader>[ :Buffers<CR>
nnoremap <Leader>f :Lines<CR>
nnoremap <Leader>bf :BLines<CR>
nnoremap <Leader>o :Files<CR>
nnoremap <Leader>p :GFiles<CR>
nnoremap <silent> <leader>/ :execute 'Ag ' . input('Search: ')<CR>
command! CommandHistory call fzf#vim#command_history({'right': '~40%'})
command! SearchHistory call fzf#vim#search_history({'right': '~40%'})
let g:fzf_action = {
			\ 'ctrl-t': 'tab split',
			\ 'ctrl-h': 'split',
			\ 'ctrl-v': 'vsplit'
			\ }
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }
" }}}
" Emmet {{{
let g:user_emmet_mode='a'
let g:user_emmet_leader_key='<C-E>'
" Enabling Emmet just for html, css, and markdown files
let g:user_emmet_install_global = 0
autocmd BufNewFile,BufRead *.html,*.css,*.md EmmetInstall
" }}}
" }}}
" Key Mappings {{{
" Real man don't use arrow keys {{{
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
" }}}
" I duno why but this line below doesn't work for me
"let mapleader = "\<Space>"
map <Space> <Leader>
map <F1> :Buffers<cr>
map <F2> :GFiles<cr>
nnoremap ; :
" Fast saving
noremap <Leader>w :w<CR>
" Fast quit
noremap <Leader>q :q<CR>
" Fast save & quit
noremap <Leader>wq :wq<CR>
" Fast quit all
noremap <Leader>qa :qa<CR>
" Buffers
noremap <Leader>bd :bd<CR>
" Switch to last buffer
nnoremap <bs> <c-^>
" Navigate trough tabs
nmap > gt
nmap < gT
" Splits
noremap <silent> <Leader>sh :new<CR>
noremap <silent> <Leader>sv :vnew<CR>
" Mirror splits
noremap <silent> <Leader>mh :split<CR>
noremap <silent> <Leader>mv :vsplit<CR>
" Easy splits navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" Indent whole file
nnoremap <Leader>= gg=G
" Toggle paste mode
set pastetoggle=<F12>
" Sort selected lines
vnoremap <F9> :sort<CR>
" Sort selected line/ words
vnoremap <F10> d:execute 'normal i' . join(sort(split(getreg('"'))), ' ')<CR>
" Aligning text
vnoremap <silent> <Leader>tc :center<CR>
vnoremap <silent> <Leader>tr :right<CR>
vnoremap <silent> <Leader>tl :left<CR>
" Git
nnoremap <silent> <F6> :Gvdiff<CR>
nnoremap <silent> <F7> :Gstatus<CR>
noremap <silent> <Leader>gd :Gvdiff<CR>
noremap <silent> <Leader>gs :Gstatus<CR>
noremap <silent> <Leader>gb :Gblame<CR>
noremap <silent> <Leader>gr :SignifyRefresh<CR>
noremap <silent> <Leader>gp :Dispatch git push<CR>
" }}}
" Commands {{{
" Prettify JSON command
command! JSONPretty %!python -m json.tool
" Clear all registers
command! WipeReg for i in range(34,122) | silent! call setreg(nr2char(i), []) | endfor
" When I forgot to open vim with sudo
command! ALV w !sudo tee % > /dev/null
" }}}
" UI {{{
" Diff mode {{{2
function DiffMode()
	if &diff
		let ayucolor="light"
		set laststatus=0
		set showtabline=0
		set diffopt+=iwhite
		set scrollbind
		set scrollopt=ver,jump,hor
		set fdm=diff
		nnoremap <Leader>q :qa<CR>
	else
		let ayucolor="dark"
	endif
endfunction
	autocmd FilterWritePre * call DiffMode()
	" }}}
if has('termguicolors')
	set termguicolors
	let ayucolor="dark"
	colorscheme ayu
endif
" Statusline {{{
let g:currentmode={
			\ 'n'      : 'Normal',
			\ 'no'     : 'N·Operator Pending',
			\ 'v'      : 'Visual',
			\ 'V'      : 'V·Line',
			\ ''     : 'V·Block',
			\ 's'      : 'Select',
			\ 'S'      : 'S·Line',
      \ '^S'     : 'S·Block',
			\ 'i'      : 'Insert',
			\ 'R'      : 'Replace',
			\ 'Rv'     : 'V·Replace',
			\ 'c'      : 'Command',
			\ 'cv'     : 'Vim Ex',
			\ 'ce'     : 'Ex',
			\ 'r'      : 'Prompt',
			\ 'rm'     : 'More',
			\ 'r?'     : 'Confirm',
			\ '!'      : 'Shell',
			\ 't'      : 'Terminal'
			\}"
function! GitBranch()
	return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction
function! StatuslineGit()
	let l:branchname = GitBranch()
	return strlen(l:branchname) > 0?' '.l:branchname.' ':''
endfunction
function! StatusReadonly()
	return (&readonly || !&modifiable) ? '' : ''
endfunction
function! StatusBuffers()
  return len(filter(range(1, bufnr('$')), 'buflisted(v:val)'))
endfunction
set statusline=
"set statusline+=%{StatuslineColor()}
set statusline+=\ %{toupper(g:currentmode[mode()])}
"set statusline+=\ %{toupper(mode())}
set statusline+=\ %{StatusReadonly()}
set statusline+=\%M
set statusline+=\ %{StatusBuffers()}:%n
set statusline+=\ %t
set statusline+=%=
"set statusline+=\ %l:%c:%p:%L
set statusline+=\ %l:%c
set statusline+=\ %{LineNoIndicator()}
set statusline+=\ %Y
"set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
"set statusline+=\/%{&ff}
set statusline+=\ %{StatuslineGit()}
set statusline+=\ %=
" }}}
set fillchars=stl:\ ,stlnc:\ ,vert:\|,fold:-,diff:-
set showbreak=↪\ ,
set listchars=tab:»\ ,extends:›,precedes:‹,trail:·,nbsp:·,eol:¬
set list
set noruler
set cursorline
set scrolloff=6
set sidescrolloff=6
set shortmess=atIO
set showtabline=2
set laststatus=2
set noruler
set noshowmode
set nowrap
set display=lastline
" Folds
set foldcolumn=0
set foldenable
set foldlevel=1
set foldmethod=indent
" Splits
set splitbelow
set splitright
" Filetype plugins
filetype plugin indent on
" Enabling syntax
syntax enable
" Resize all open splits/windows proportionally when terminal is resized
autocmd VimResized * :wincmd =
" Just current buffer/window with cursorline
augroup cursor_line
	autocmd!
	autocmd VimEnter * setlocal cursorline
	autocmd WinEnter * setlocal cursorline
	autocmd BufEnter * setlocal cursorline
	autocmd WinLeave * setlocal nocursorline
augroup END
" Toggle number {{{
nnoremap <silent> <Leader>n :set number!<CR>
nnoremap <silent> <Leader>nn :set relativenumber!<CR>
"}}}
"}}}
" Clipboard {{{
if has("unix") && has("clipboard")
	let s:uname = system("uname")
	if s:uname == "Darwin\n"
		set clipboard=unnamed
	elseif s:uname == "Linux\n"
		set clipboard=unnamedplus
	endif
endif
" }}}
